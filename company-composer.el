;;; company-composer.el --- Company mode backend for composer -*- lexical-binding: t; -*-
;; Copyright (C) 2020  Fermin Munoz

;; Author: Fermin MF <fmfs@posteo.net>
;; Created: 4 October 2020
;; Version: 0.0.1
;; Keywords: company,php,convenience,auto-completion

;; URL: https://gitlab.com/sasanidas/company-plisp
;; Package-Requires: ((emacs "25") (s "1.2.0") (company "0.8.12") (cl-lib "0.5"))
;; License: GPL-3.0-or-later

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Backend for company mode for composer dependencies.

;;; Code:

(require 's)
(require 'cl-lib)
(require 'company)


(defgroup company-composer nil
  "Company mode backend for composer require."
  :group 'company
  :prefix "company-composer-"
  :link '(url-link :tag "Repository" ""))


(defcustom company-composer-exec "composer"
  "Composer location."
  :type 'file
  :group 'company-composer)

(defcustom company-composer-cli-arguments "-n --ansi"
  "Composer command line arguments."
  :type 'string
  :group 'company-composer)

(defun company-composer--get-completion (arg)
  "Call composer search to get completion with ARG prefix."
  (remove "" (seq-map (lambda (element)
			;; (let* ((suggestion (substring element 0 (string-search "\s" element )) )
			;; 	    (suggestion-doc (substring element (string-search "\s" element ))))
			;;   (cons suggestion suggestion-doc)
			;;   )
			(substring element 0 (string-search "\s" element )))
		      (s-lines
		       (shell-command-to-string (format "%s search %s %s"
							company-composer-exec company-composer-cli-arguments arg))))))

;; (defun company-composer-doc (symbol)
;;   "Get documentation from SYMBOL."
;;   )

(defun company-composer (command &optional arg &rest ignored)
  "Company composer main function, it requires COMMAND."
  (interactive (list 'interactive))
  (cl-case command
    (interactive (company-begin-backend 'company-composer))
    (prefix (and (eq major-mode 'yaml-mode) (company-in-string-or-comment)
		 (company-grab-symbol)))
    (candidates (company-composer--get-completion arg))
    )
  )


(provide 'company-composer)
;;; company-composer.el ends here
